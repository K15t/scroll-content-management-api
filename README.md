# Description

This repository contains the public Java API for these Scroll Content Management apps:
* [Scroll Versions](https://marketplace.atlassian.com/apps/1210818/scroll-versions-for-confluence)
* [Scroll Translations](https://marketplace.atlassian.com/apps/1211616/scroll-translations-for-confluence)

Binary, Javadoc and source artifacts are published through our Maven repository. See below for instructions how to access them.

The API project itself uses [semantic versioning](https://semver.org), the apps however do not.

# License
The Scroll Content Management public APIs are licensed under the terms of the *MIT license*.

# Usage

The public APIs can be used by adding a `provided` Maven dependency to the project. At runtime this will be provided by the *Scroll Platform* plugin, which is bundled and installed by any of the Scroll Content Management apps (*Scroll Versions*, *Scroll Translations* and *Scroll Acrolinx Connector*).

First you'll need to add the K15t Maven repository to your project or Maven settings:
```xml
<repositories>
    <repository>
        <id>k15t</id>
        <url>https://nexus.k15t.com/content/repositories/releases</url>
    </repository>
</repositories>
```

Then add this dependency to the `pom.xml` of your project:
```xml
<dependencies>
    ...

    <dependency>
        <groupId>com.k15t.scroll</groupId>
        <artifactId>scroll-content-management-api</artifactId>
        <version>1.0.0</version>
        <scope>provided</scope>
    </dependency>

    ...
</dependencies>
```

Finally add the following OSGi imports to your `maven-confluence-plugin` configuration in your `pom.xml`:
```xml
<plugin>
    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>maven-confluence-plugin</artifactId>
    <configuration>
        ...
        <instructions>
            <Import-Package>
                com.k15t.scroll.scm.api.*;version="[1.0.0,2.0.0)",
                ...
            </Import-Package>
            ...        
        </instructions>
        ...
    </configuration>
</plugin>
```
The version range `[1.0.0,2.0.0)` declares that your plugin is compatible with all versions of the API starting from (and including) version 1.0.0 up to (but NOT including) 2.0.0.

A new major version of the API will have breaking changes, so it is very probable that you'll need to adapt your source code. Therefore you should not assume that your code will work out of the box with the next major version of the API.

We recommend to declare the version range as follows:

* Use the version of the maven dependency as the lower bound
* Use the next major version as the **excluded** upper bound

