package com.k15t.scroll.scm.api;

import java.time.LocalDate;


/**
 * This class represents a version managed by Scroll Versions in a single space.
 */
public interface Version {

    String getId();


    String getName();


    String getDescription();


    /**
     * Determines if a version is hidden. A hidden version doesn't show up in the Versions dropdown on a page.
     *
     * @return true if the version is hidden.
     */
    boolean isHidden();


    String getPrecedingVersionId();


    LocalDate getReleaseDate();


    String getSpaceKey();

}
