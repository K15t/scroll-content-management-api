package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a version is created in a space.
 */
public interface VersionCreateEvent extends SpaceEvent {

    Version getVersion();

}
