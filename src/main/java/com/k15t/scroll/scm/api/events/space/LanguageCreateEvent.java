package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Language;


/**
 * This event is published when a language is created in a space.
 */
public interface LanguageCreateEvent extends SpaceEvent {

    Language getLanguage();

}
