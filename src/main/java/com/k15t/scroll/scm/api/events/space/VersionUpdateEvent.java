package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a version is updated in a space.
 */
public interface VersionUpdateEvent extends SpaceEvent {

    Version getOldVersion();


    Version getNewVersion();

}
