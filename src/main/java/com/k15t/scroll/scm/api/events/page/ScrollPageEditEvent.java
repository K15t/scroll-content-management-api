package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.Language;

import java.util.Optional;


/**
 * This event is published whenever a Scroll Page is edited through the Confluence Editor.
 */
public interface ScrollPageEditEvent extends PageEvent {

    /**
     * Get the language the Scroll Page was edited in.
     *
     * @return an Optional containing the language the page was edited in, or an empty Optional if no languages are configured in the space.
     */
    Optional<Language> getEditedLanguage();

}
