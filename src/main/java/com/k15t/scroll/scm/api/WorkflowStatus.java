package com.k15t.scroll.scm.api;

/**
 * This enum represents the different workflow status a page can have when using the Scroll Versions built-in workflow.
 */
public enum WorkflowStatus {

    /**
     * The page is currently worked on and it can be submitted for {@link WorkflowStatus#approval}.
     */
    draft,

    /**
     * The page is currently under review. It can be rejected to {@link WorkflowStatus#draft} or accepted to {@link WorkflowStatus#done}.
     */
    approval,

    /**
     * The page is done and needs no further work. The workflow can be restarted to {@link WorkflowStatus#draft}.
     */
    done

}
