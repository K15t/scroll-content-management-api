package com.k15t.scroll.scm.api;

import java.util.List;


/**
 * This class represents an attribute managed by Scroll Versions. Attributes are used to define Variants.
 */
public interface Attribute {

    String getId();


    String getName();


    String getDescription();


    List<String> getValues();


    String getSpaceKey();

}
