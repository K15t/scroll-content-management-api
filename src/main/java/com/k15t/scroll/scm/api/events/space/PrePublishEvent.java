package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Language;
import com.k15t.scroll.scm.api.PublishType;
import com.k15t.scroll.scm.api.Variant;
import com.k15t.scroll.scm.api.Version;

import java.util.List;
import java.util.Optional;


/**
 * This event is published at the beginning of a publishing process. It is published after the initial setup, e.g. creating an empty
 * space when publishing to a new space, but before the pages actually get published.
 */
public interface PrePublishEvent {

    /**
     * Get the published version.
     *
     * @return an Optional containing the published version.
     */
    Optional<Version> getPublishedVersion();


    /**
     * Get the published languages.
     *
     * @return a list containing all published languages. Empty list when no languages are active in the source space.
     */
    List<Language> getPublishedLanguages();


    /**
     * Get the published variants. When publishing "all" variants, the list will only contain one entry: The "all" variant.
     *
     * @return a list of all published variants.
     */
    List<Variant> getPublishedVariants();


    /**
     * Get the source space key.
     *
     * @return the space key of the space that gets published.
     */
    String getSourceSpaceKey();


    /**
     * Get the target space key.
     *
     * @return the space key of the space that gets published into. When publishing within the same, this will be the same as the source
     * space key.
     */
    String getTargetSpaceKey();


    /**
     * Get the publish type.
     *
     * @return the type of the publishing process.
     */
    PublishType getPublishType();


    /**
     * When publishing to another system with the
     * <a href="https://marketplace.atlassian.com/apps/1212746/scroll-remote-publishing-endpoint">Scroll Remote Publishing Endpoint</a>,
     * this method gets the ID of the application link to the other system.
     * The application link can be retrieved through the com.atlassian.applinks.api.ApplicationLinkService component.
     *
     * @return an Optional containing the ID of the application link, or an empty Optional when publishing within the same system.
     */
    Optional<String> getTargetApplicationLinkId();

}
