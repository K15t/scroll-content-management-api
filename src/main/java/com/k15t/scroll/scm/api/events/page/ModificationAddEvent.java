package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a page has been added to a version.
 * This means that this event is either published by creating a completely new versioned page, editing a page in a version it hasn't been
 * edited before or when converting a {@link com.k15t.scroll.scm.api.ScrollPage.Type#removePage} to a
 * {@link com.k15t.scroll.scm.api.ScrollPage.Type#modificationPage}.
 * The page is added as a {@link com.k15t.scroll.scm.api.ScrollPage.Type#modificationPage}
 */
public interface ModificationAddEvent extends PageEvent {

    Version getNewVersion();

}
