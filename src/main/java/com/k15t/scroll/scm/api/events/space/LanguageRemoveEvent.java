package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Language;


/**
 * This event is published when a language is removed in a space.
 */
public interface LanguageRemoveEvent extends SpaceEvent {

    Language getLanguage();

}
