package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a {@link com.k15t.scroll.scm.api.ScrollPage.Type#unversionedPage} is converted to a
 * {@link com.k15t.scroll.scm.api.ScrollPage.Type#modificationPage}.
 */
public interface ConvertToVersionedEvent extends PageEvent {

    /**
     * @return the {@link Version} of the new modification page.
     */
    Version getNewVersion();

}
