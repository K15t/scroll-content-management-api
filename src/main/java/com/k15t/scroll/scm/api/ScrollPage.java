package com.k15t.scroll.scm.api;

/**
 * A ScrollPage represents a page in a space with either versions, variants or languages. It is an abstraction that hides away the
 * implementation details how versioned, conditional and translated content is maintained by the system and always represents the content
 * in one version.
 * <ul>
 * <li>In versioned spaces a ScrollPage may represent one of up to one Confluence page per version. If no Confluence page exists for a
 * specific version, the content of the preceding version's page is inherited.</li>
 * <li>In translated spaces all translations are maintained on a single Confluence page.</li>
 * <li>All Confluence pages belonging to the same ScrollPage share the same Scroll Page ID.</li>
 * </ul>
 */
public interface ScrollPage {

    enum Type {

        /**
         * A masterpage is the root Confluence page of a versioned Scroll Page. The masterpage is always viewed in the public view and is
         * used to present published content when publishing in the same space.
         */
        masterPage,

        /**
         * An unversioned page is a page which is the same in all versions - the Scroll Page is a single Confluence page.
         */
        unversionedPage,

        /**
         * A modification page is a Confluence page which exists in the version it is assigned to - it represents a change (modification)
         * in that version.
         * A modification page is always the child of its master page.
         */
        modificationPage,

        /**
         * A fallback page is a Confluence page which is inherited from a preceding version because the Scroll Page has not yet been
         * edited in the version of the fallback page.
         */
        fallbackPage,

        /**
         * An unresolved Page is a page which doesn't exist in the current version because the Scroll Page has been added in a later
         * version and therefore there is no Confluence page to inherit.
         * An unresolved page points to it's master page.
         */
        unresolvedPage,

        /**
         * A remove page is a Confluence page which represents a removed page in this specific version.
         */
        removePage
    }


    /**
     * Get the page id of the Confluence page representing the Scroll Page in this version. See {@link ScrollPage.Type}
     *
     * @return the page id of the Confluence page representing the Scroll Page.
     */
    long getConfluencePageId();


    /**
     * Get the Scroll Page Id. The Scroll Page Id identifies all Confluence pages of a single Scroll Page. This means that the master
     * page and all it's modification pages and remove pages share the same Scroll Page Id.
     *
     * @return the Scroll Page Id of the Scroll Page.
     */
    String getScrollPageId();


    /**
     * Get the Scroll Page Title of the Scroll Page. The Scroll Page Title is the title displayed in the UI, not the Confluence page title.
     * When there are multiple languages in a space this method will always return the title in the default language.
     *
     * @return the Scroll Page Title of the Scroll Page.
     */
    String getScrollPageTitle();


    String getScrollPageKey();


    String getScrollPageKeyUrlPath();


    String getSpaceKey();


    Version getVersion();


    Type getType();

}
