package com.k15t.scroll.scm.api;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;


/**
 * This service is responsible for handling translations of pages in spaces with multiple languages.
 */
public interface TranslationService {

    /**
     * Converts the content of the default language of a given page to XLIFF.
     *
     * @param confluencePageId the page id of the page of which the content should be converted to XLIFF.
     * @param targetLanguageKey the "target-language" in the created XLIFF. This is the language that it will be translated to when
     * importing the XLIFF.
     * @param target a stream to write the created XLIFF to.
     */
    void createXliffForPage(long confluencePageId, String targetLanguageKey, OutputStream target);


    /**
     * Converts XLIFF into a page translation by converting the translation in XLIFF into Confluence Storage format and adding metadata
     * like the translated title and Confluence page id to the page translation.
     *
     * @param xliffData the XLIFF to convert to a page translation.
     * @return the page translation converted from XLIFF.
     */
    List<PageTranslation> convertXliffToPageTranslations(InputStream xliffData);


    /**
     * Import a page translation. The content of the translation will be imported to the Confluence page and language specified on the
     * page translation.
     *
     * @param pageTranslation the page translation to import.
     * @param translationStatus the translation status of the page in the imported language after importing the translation.
     * @param notifyWatchers notify watchers of the page when true.
     */
    void importPageTranslation(PageTranslation pageTranslation, TranslationStatus translationStatus, boolean notifyWatchers);

}
