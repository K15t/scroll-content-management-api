package com.k15t.scroll.scm.api;

/**
 * This enum represents the different types of publishing that are possible with Scroll Versions.
 */
public enum PublishType {

    /**
     * Publishing a space to itself.
     */
    to_same,

    /**
     * Publishing a space to a new space which gets created in the publishing process.
     */
    to_new,

    /**
     * Publishing a space to another space which already exists before publishing.
     */
    to_existing
}
