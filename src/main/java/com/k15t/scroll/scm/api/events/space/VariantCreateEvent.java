package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Variant;


/**
 * This event is published when a variant is created in a space.
 */
public interface VariantCreateEvent extends SpaceEvent {

    Variant getVariant();

}
