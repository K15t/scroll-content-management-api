package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a {@link com.k15t.scroll.scm.api.ScrollPage.Type#modificationPage} is converted to a
 * {@link com.k15t.scroll.scm.api.ScrollPage.Type#unversionedPage}.
 */
public interface ConvertToUnversionedEvent extends PageEvent {

    /**
     * @return the {@link Version} from which the page was converted to an
     * {@link com.k15t.scroll.scm.api.ScrollPage.Type#unversionedPage} page.
     */
    Version getOldVersion();

}
