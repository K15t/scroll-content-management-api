package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.ScrollPage;


/**
 * This interface indicates that an event is published in the context of a page.
 */
public interface PageEvent {

    ScrollPage getScrollPage();

}
