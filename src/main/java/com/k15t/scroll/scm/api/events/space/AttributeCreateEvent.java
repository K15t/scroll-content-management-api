package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Attribute;


/**
 * This event is published when an attribute is created in a space.
 */
public interface AttributeCreateEvent extends SpaceEvent {

    Attribute getAttribute();

}
