package com.k15t.scroll.scm.api;

/**
 * This class represents the translation of a page in a specific language and is created from an imported XLIFF file.
 */
public interface PageTranslation {

    /**
     * Get the Confluence page id of the page this translation belongs to.
     *
     * @return the Confluence page id.
     */
    long getConfluencePageId();


    int getConfluencePageRevision();


    /**
     * Get the translated title.
     * If the page title has not been translated in the imported XLIFF file it will fall back to the title in the default language.
     *
     * @return the translated title.
     */
    String getTitle();


    /**
     * Get the translated content.
     *
     * @return the translated content.
     */
    String getContent();


    String getScrollPageId();


    /**
     * Get the id of the (Scroll) version of this page.
     *
     * @return the id of the version of the page. If there are no versions in the space of the page, returns "current".
     */
    String getVersionId();


    /**
     * Get the language key of the translation.
     *
     * @return the language key which specifies the language of the title and content of the translation.
     */
    String getLanguageKey();

}
