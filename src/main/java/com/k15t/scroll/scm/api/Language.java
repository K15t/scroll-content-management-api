package com.k15t.scroll.scm.api;

import java.util.Locale;


/**
 * This class represents a language in Scroll Translations. Languages are defined on a space level.
 */
public interface Language {

    String getKey();


    boolean isDefault();


    String getSpaceKey();


    Locale getLocale();

}
