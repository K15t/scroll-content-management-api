package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.Language;
import com.k15t.scroll.scm.api.TranslationStatus;


/**
 * This event is published when the translation status of a page is updated.
 */
public interface TranslationStatusChangeEvent extends PageEvent {

    /**
     * @return the language of which the translation status has been updated.
     */
    Language getLanguage();


    TranslationStatus getOldStatus();


    TranslationStatus getNewStatus();

}
