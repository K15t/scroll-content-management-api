package com.k15t.scroll.scm.api;

/**
 * This service is responsible for retrieving the space roles of spaces managed by Scroll Versions and/or Scroll Translations.
 */
public interface SpaceRolesService {


    /**
     * Get the effective space roles of a space.
     * <p>
     * The effective space roles are calculated as followed:
     * <p>
     * Authors:
     * - when no authors are configured, every user with edit permissions in the space is an author.
     * - when authors are configured in the space, those are the authors.
     * - all Confluence administrators are automatically authors.
     * <p>
     * Reviewers:
     * - when no reviewers and no authors are configured, every user with view permissions in the space is a reviewer.
     * - when reviewers are configured in the space, those are the reviewers.
     * - all Confluence administrators are automatically reviewers.
     * <p>
     * Translators:
     * - when no translators and no authors are configured, every user with edit permissions in the space is a translator.
     * - when translators are configured in the space, those are the translators.
     * - all Confluence administrators are automatically authors.
     * <p>
     * Doc-Admins:
     * - all space admins are automatically doc-admins.
     * - all users configured as doc-admins are doc-admins.
     * - all Confluence administrators are automatically doc-admins.
     *
     * @param spaceKey the space key of the space to retrieve the space roles from
     * @return the effective space roles
     */
    SpaceRoles getEffectiveSpaceRoles(String spaceKey);


    /**
     * Get the configured space roles of a space. The configured space roles are the space roles configured under
     * Space Tools &gt; Scroll Add-ons &gt; Administration &gt; Permissions.
     *
     * @param spaceKey the space key of the space to retrieve the space roles from
     * @return the configured space roles
     */
    SpaceRoles getConfiguredSpaceRoles(String spaceKey);

}
