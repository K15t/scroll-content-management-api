package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.SpaceRoles;
import com.k15t.scroll.scm.api.SpaceRolesService;


/**
 * This event is published when the space roles in a space are changed.
 */
public interface SpaceRolesConfigChangeEvent extends SpaceEvent {

    /**
     * Get the old space roles.
     * Those are the configured space roles, see {@link SpaceRolesService#getConfiguredSpaceRoles(String)}.
     * @return the old space roles.
     */
    SpaceRoles getOldConfiguredSpaceRoles();


    /**
     * Get the updated space roles.
     * Those are the configured space roles, see {@link SpaceRolesService#getConfiguredSpaceRoles(String)}.
     * @return the new space roles.
     */
    SpaceRoles getNewConfiguredSpaceRoles();

}
