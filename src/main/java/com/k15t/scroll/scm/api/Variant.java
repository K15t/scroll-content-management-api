package com.k15t.scroll.scm.api;

import java.util.List;


/**
 * This class represents a variant managed by Scroll Versions. Variants are used to make content conditional.
 */
public interface Variant {

    String getId();


    String getName();


    String getDescription();


    /**
     * @return a list of conditions which need to be fulfilled for content to be in this variant.
     */
    List<Condition> getConditions();


    String getSpaceKey();

}
