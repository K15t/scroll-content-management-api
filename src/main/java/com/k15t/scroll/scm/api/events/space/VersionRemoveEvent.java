package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a version is removed in a space.
 */
public interface VersionRemoveEvent extends SpaceEvent {

    Version getVersion();

}
