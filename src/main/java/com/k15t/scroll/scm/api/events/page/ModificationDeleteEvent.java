package com.k15t.scroll.scm.api.events.page;


import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a page has been marked as removed in a version.
 * It is converted to a {@link com.k15t.scroll.scm.api.ScrollPage.Type#removePage}
 */
public interface ModificationDeleteEvent extends PageEvent {

    /**
     * @return the {@link Version} in which the page has been marked as removed.
     */
    Version getVersion();

}
