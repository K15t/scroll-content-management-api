package com.k15t.scroll.scm.api.events.page;


import com.k15t.scroll.scm.api.WorkflowStatus;


/**
 * This event is published when the workflow status of a page is updated. It is only published when using the "simple" workflow which is
 * built in Scroll Versions. It is not published when using the Comala Workflows Integration.
 * For which events are thrown when using the Comala Workflows Integration have a look at the
 * <a href="https://wiki.comalatech.com/display/CWL/Workflow+Events">Comala Workflows Documentation</a>.
 */
public interface WorkflowStatusChangeEvent extends PageEvent {

    WorkflowStatus getOldStatus();


    WorkflowStatus getNewStatus();

}
