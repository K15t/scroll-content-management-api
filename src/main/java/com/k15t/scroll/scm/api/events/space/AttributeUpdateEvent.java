package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Attribute;


/**
 * This event is published when an attribute is updated in a space.
 */
public interface AttributeUpdateEvent extends SpaceEvent {

    Attribute getOldAttribute();


    Attribute getNewAttribute();

}
