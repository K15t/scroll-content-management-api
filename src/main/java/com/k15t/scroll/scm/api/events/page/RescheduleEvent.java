package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a {@link com.k15t.scroll.scm.api.ScrollPage.Type#modificationPage} has been rescheduled to a different
 * version.
 */
public interface RescheduleEvent extends PageEvent {

    /**
     * @return the {@link Version} from which the page was rescheduled.
     */
    Version getFromVersion();


    /**
     * @return the {@link Version} to which the page was rescheduled.
     */
    Version getToVersion();

}
