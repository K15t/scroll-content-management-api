package com.k15t.scroll.scm.api.events.space;


/**
 * This interface indicates that an event is published in the context of a space.
 */
public interface SpaceEvent {

    String getSpaceKey();

}
