package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.Version;


/**
 * This event is published when a versioned Confluence page is removed (either a
 * {@link com.k15t.scroll.scm.api.ScrollPage.Type#modificationPage} or a {@link com.k15t.scroll.scm.api.ScrollPage.Type#removePage}).
 * Once the page has been removed it will be a {@link com.k15t.scroll.scm.api.ScrollPage.Type#fallbackPage} or a
 * {@link com.k15t.scroll.scm.api.ScrollPage.Type#unresolvedPage} because the Confluence page doesn't exist anymore.
 */
public interface RemoveFromVersionEvent extends PageEvent {

    /**
     * @return the {@link Version} which the page was removed from.
     */
    Version getVersion();

}
