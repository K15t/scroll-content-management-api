package com.k15t.scroll.scm.api;

/**
 * This enum represents the different translation status a page can have in a single language with Scroll Translations.
 */
public enum TranslationStatus {

    not_translated, outdated, translated

}
