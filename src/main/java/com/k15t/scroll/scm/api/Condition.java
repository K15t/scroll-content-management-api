package com.k15t.scroll.scm.api;

import java.util.List;


/**
 * This class represents an attribute-specific condition of a variant.
 * Content needs to fulfill all conditions of a variant to be considered as available in that variant.
 */
public interface Condition {

    /**
     * This flag indicates if the condition is also fulfilled when no values are set for the attribute at all.
     * @return true if the condition is also fulfilled with no values set, else false.
     */
    boolean acceptContentWithoutValues();


    /**
     * @return the attribute this condition belongs to.
     */
    Attribute getAttribute();


    /**
     * @return the required attribute values which need to be set for this condition to be fulfilled.
     */
    List<String> getRequiredAttributeValues();

}
