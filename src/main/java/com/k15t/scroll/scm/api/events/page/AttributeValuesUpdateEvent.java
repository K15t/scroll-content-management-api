package com.k15t.scroll.scm.api.events.page;

import com.k15t.scroll.scm.api.Attribute;

import java.util.List;
import java.util.Map;


/**
 * This event is published when the attribute values of a page have changed.
 */
public interface AttributeValuesUpdateEvent extends PageEvent {

    /**
     * Get the attribute values assigned to the page before the update was performed.
     * @return a map which is structured as followed:
     *      The key is the attribute, the value is a list of all values of the key's attribute set on the page.
     */
    Map<Attribute, List<String>> getOldAttributeValues();


    /**
     * Get the new attribute values assigned to the page.
     * @return a map which is structured as followed:
     *      The key is the attribute, the value is a list of all values of the key's attribute set on the page.
     */
    Map<Attribute, List<String>> getNewAttributeValues();

}
