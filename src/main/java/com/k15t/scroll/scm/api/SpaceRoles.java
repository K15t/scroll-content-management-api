package com.k15t.scroll.scm.api;

import java.util.Set;


/**
 * This class represents the space roles of Scroll Versions and Scroll Translations.
 */
public interface SpaceRoles {

    Set<String> getDocAdminUserKeys();


    Set<String> getDocAdminGroupNames();


    Set<String> getAuthorUserKeys();


    Set<String> getAuthorGroupNames();


    Set<String> getReviewerUserKeys();


    Set<String> getReviewerGroupNames();


    Set<String> getTranslatorUserKeys();


    Set<String> getTranslatorGroupNames();

}
