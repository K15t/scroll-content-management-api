package com.k15t.scroll.scm.api.events.space;

import com.k15t.scroll.scm.api.Variant;


/**
 * This event is published when a variant is updated in a space.
 */
public interface VariantUpdateEvent extends SpaceEvent {

    Variant getOldVariant();


    Variant getNewVariant();

}
